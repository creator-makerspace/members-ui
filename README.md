## chieftain-ui

WIP...

This is a small portal for the Creator Makerspace Chieftain member system.

### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed
- run `npm install` to install dependencies
- run `npm run server` to fire up dev server
- open browser to `http://localhost:8080`


## Dependency notes

https://github.com/valor-software/ng2-bootstrap
http://valor-software.github.io/ng2-bootstrap/index-bs4.html

## Dependency injection
http://www.syntaxsuccess.com/viewarticle/dependency-injection-in-angular-2.0 < Go read
