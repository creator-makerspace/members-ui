import {Component} from 'angular2/core';

import {ProfileService} from '../../services/profile';
import {GravatarProfile} from '../gravatar/gravatar';
import {StateIcon} from '../state-icon/state-icon';

@Component({
  selector: "profile-home",
  template: require('./profile-home.html'),
  styles: [require('./profile-home.css')],
  directives: [GravatarProfile, StateIcon],
  providers: []
})
export class Home {
  integration: any;
  profile = {};

  constructor(private profileSvc: ProfileService) {
    profileSvc.current.subscribe(
      data => {
        this.profile = data;
      }
    );

    this.profileSvc.refreshProfile();
  }
}
