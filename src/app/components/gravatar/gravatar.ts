import { Input, Component, View } from 'angular2/core';
var md5 = require("md5");

var gravatarTmpl = require("./gravatar.html");

@Component({
  selector: 'gravatar'
})
@View({
  template: gravatarTmpl
})
export class GravatarProfile {
  @Input() email: string = null;
  @Input() imgSize: number = 300;
  @Input() imgClasses: string = "";

  gravatarHost: string = "http://www.gravatar.com/avatar";
  gravatarId: string = null;
  imageSrc: string = null;

  ngOnInit() {
    if (this.email === undefined || this.email === null) {
      return;
    }
    this.setProfile(this.email);
  }

  ngOnChanges(changes) {
    if (this.email !== null) {
      this.setProfile(this.email);
    }
  }

  setId(email: string) {
    this.gravatarId = md5(email);
  }

  setImageSrc() {
    this.imageSrc = `${this.gravatarHost}/${this.gravatarId}?s=${this.imgSize}`;
  }

  setProfile(email: string) {
    this.setId(email);
    this.setImageSrc();
  }
}
