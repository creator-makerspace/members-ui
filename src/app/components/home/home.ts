import {Component} from 'angular2/core';
import { ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
  selector: 'home',
  template: require('./home.html'),
  styles: [require('./home.css')],
  providers: [],
  directives: [ROUTER_DIRECTIVES],
  pipes: []
})
export class Home {

}
