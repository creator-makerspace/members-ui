import {Component} from 'angular2/core';

@Component({
  selector: 'about',
  template: require('./about.html'),
  styles: [require('./about.css')],
  providers: [],
  directives: [],
  pipes: []
})
export class About {
}
