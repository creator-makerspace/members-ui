import {Component} from 'angular2/core';
import {Http} from 'angular2/http';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {Dropdown} from 'ng2-bootstrap/ng2-bootstrap';

import {OpenID} from '../../services/openid';
import {ProfileService} from '../../services/profile';

import {GravatarProfile} from '../gravatar/gravatar';


@Component({
  selector: "navbar",
  template: require('./navbar.html'),
  styles: [require('./navbar.css')],
  directives: [ROUTER_DIRECTIVES, Dropdown, GravatarProfile],
  providers: []
})
export class Navbar {
  profile = {};
  private status: { isopen: boolean } = { isopen: false };

  constructor(private openid: OpenID, public $http: Http, private profileService: ProfileService) {
    profileService.current.subscribe(
      data => {
        this.profile = data;
      }
    );
  }

  toggleDropdown($event: MouseEvent) {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
}
