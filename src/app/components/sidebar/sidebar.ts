import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

import {OpenID} from '../../services/openid';

@Component({
  selector: 'sidebar',
  template: require('./sidebar.html'),
  styles: [require('./sidebar.css')],
  providers: [],
  directives: [ROUTER_DIRECTIVES],
  pipes: []
})
export class Sidebar {
  private status: {isopen: boolean} = {isopen: false};

  constructor(private openid: OpenID) {
  }
}
