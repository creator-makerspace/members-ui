import { Component, View } from 'angular2/core';
import { RouterLink } from 'angular2/router';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from 'angular2/common';

import {OpenID} from '../../services/openid';

let styles = require('./login.css');
let template = require('./login.html');

@Component({
  selector: 'login',
  providers: []
})
@View({
  directives: [RouterLink, CORE_DIRECTIVES, FORM_DIRECTIVES],
  template: template,
  styles: [styles],
})
export class Login {
  openidUrl: string;

  constructor(public openid: OpenID) {}

  ngOnInit() {
    var self = this;

    // Set the URL we'll link to
    this.openid.discover().subscribe(
      data => {
        if (data !== undefined) {
          this.openidUrl = self.openid.getAuthorizeUrl();
        }
      }
    );
  }
}
