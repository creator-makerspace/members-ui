import { Component } from 'angular2/core';
import { Router } from 'angular2/router';

import {OpenID} from '../../services/openid';
import {ProfileService} from '../../services/profile';

/* Controller to facilitate reception of the openid redirect */
@Component({
  template: "Please wait....",
  providers: []
})
export class OpenIDCmpt {
  constructor(
    private router: Router,
    private openid: OpenID,
    private profile: ProfileService
  ) {
  }

  ngOnInit() {
    if (window.location.search !== "") {
      this.openid.setTokenFromQuery();
      this.router.navigateByUrl("/");
    }
  }
}
