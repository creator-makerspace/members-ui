import { Component } from 'angular2/core';
import { Router } from 'angular2/router';

import {Observable} from "rxjs";

import {OpenID} from '../../services/openid';

let logout = require("./logout.html");

@Component({
  selector: 'logout',
  template: logout,
  providers: []
})
export class Logout {
  constructor(public openid: OpenID, public router: Router) { }

  ngOnInit() {
    var self = this;

    if (this.openid.jwt !== null) {
      this.openid.logOut();
      Observable.timer(1000).subscribe(
        () => self.router.parent.navigateByUrl('/')
      );
    }
  }
}
