import {Component} from 'angular2/core';
import {CORE_DIRECTIVES} from "angular2/common";
import {Http} from 'angular2/http';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {DROPDOWN_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

import {APIClient} from '../../services/apiclient';


@Component({
  selector: "profile-payments",
  template: require('./overview.html'),
  styles: [require('./overview.css')],
  providers: [APIClient],
  directives: [CORE_DIRECTIVES, DROPDOWN_DIRECTIVES, ROUTER_DIRECTIVES]
})
export class InvoiceOverview  {
  isLoading: boolean = true;

  invoices: Array<any>;
  invoicesLoading: boolean = true;
  private status: {isopen: boolean} = {isopen: false};

  constructor(private http: Http, private api: APIClient) {
    api.get("/invoices")
      .map(res => res.json().invoices)
      .subscribe(
        data => {
          this.invoicesLoading = false;
          this.invoices = data;
        }
      );
  }
}
