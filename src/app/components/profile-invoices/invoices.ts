import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {InvoiceOverview} from "./overview";
import {InvoiceSettle} from "./settle";

@Component({
  template: `<router-outlet></router-outlet>`,
  directives: [ROUTER_DIRECTIVES]
})
@RouteConfig([
  {path: "/overview", component: InvoiceOverview, name: "Overview"},
  {path: "/:invoiceNr/settle", component: InvoiceSettle, name: "Settle"}
])

export class Invoices {

}
