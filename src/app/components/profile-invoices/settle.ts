import {Component, NgZone} from 'angular2/core';
import {APIClient} from '../../services/apiclient';
import {RouteParams, CanDeactivate} from 'angular2/router';

declare var braintree: any;

@Component({
  template: require('./settle.html'),
  providers: [APIClient]
})
export class InvoiceSettle implements CanDeactivate {
  btIntegration: any = null;

  invoice: any = {};
  sale: any = {};
  isLoading: boolean = true;
  isDone: boolean = false

  constructor(private api: APIClient, private routeParams: RouteParams, private zone: NgZone) {
    api.get("/invoices/" + this.routeParams.params["invoiceNr"])
      .map(res => res.json())
      .subscribe(
        data => {
          this.sale = data.sale;
          this.invoice = data.invoice;
          this.isLoading = false;

          if (!this.invoice.paid) {
            this.startBt();
          }
        }
      );
  }

  startBt() {
    var self = this;

    this.api.get('/braintree/token').subscribe(
      data => {
        braintree.setup(data.text(), "dropin", {
          container: "payment-form",
          paypal: {
            singleUse: true,
            currency: "NOK",
            amount: this.invoice.net / 100,
          },
          onReady: function (integration) {
            self.btIntegration = integration;
            self.zone.run(() => {}) // If we dont call .run() the change made from the BT lib wont be reflected..
          },
          onPaymentMethodReceived: payload => {
            payload.invoiceNumber = this.sale.identifier;

            this.api.post("/braintree/checkout", payload).subscribe(
              response => {
                this.isDone = true
              }
            );
          }
        });
      }
    );
  }

  routerCanDeactivate() {
    var self = this;

    if (self.btIntegration === null) {
      return;
    }

    return new Promise(function(resolve, reject) {
      self.btIntegration.teardown(function () {
        self.btIntegration = null;
        resolve();
      });
    });
  }

}
