import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {Invoices} from '../profile-invoices/invoices';
import {Home} from '../profile-home/profile-home';

@Component({
  selector: "profile",
  template: require('./profile.html'),
  styles: [require('./profile.css')],
  directives: [ROUTER_DIRECTIVES]
})
@RouteConfig([
  {
    path: "/home", component: Home, name: "Home"
  },
  {
    path: "/invoices/...", component: Invoices, name: "Invoices"
  }
])
export class Profile {
}
