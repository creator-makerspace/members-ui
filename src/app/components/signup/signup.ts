import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {
  FORM_DIRECTIVES,
  FormBuilder,
  ControlGroup,
  Validators,
  AbstractControl
} from 'angular2/common';

function emailValidate(control) {
  var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (!re.test(control.value)) {
    return { invalidEmail: true };
  }
}

@Component({
  template: require('./signup.html'),
  styles: [require('./signup.css')],
  directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES],
})
export class Signup {
  myForm: ControlGroup;
  firstName: AbstractControl;
  lastName: AbstractControl;
  email: AbstractControl;
  phone: AbstractControl;
  birthDate: AbstractControl;

  constructor(fb: FormBuilder) {
    this.myForm = fb.group({
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'email': ['', Validators.compose([emailValidate])],
      'phone': ["", Validators.required],
      'birthDate': ["", Validators.required],
    });

    this.firstName = this.myForm.controls["firstName"];
    this.lastName = this.myForm.controls["lastName"];
    this.email = this.myForm.controls["email"];
    this.phone = this.myForm.controls["phone"];
    this.birthDate = this.myForm.controls["birthDate"];
  }

  onSubmit(value: any): void {
    console.log('you submitted value: ', value);
  }
}
