import {Component, Input} from "angular2/core";


@Component({
  selector: "state-icon",
  template: `<i class="fa fa-{{icon}}"></i>`
})
export class StateIcon {
  @Input() stateString: string;
  icon: string = null;

  stateToIcon = {
    "ACTIVE": "ok",
    "PENDING": "question",
    "DISABLED": "minus"
  };

  constructor() {
    this.icon = this.stateToIcon[this.stateString];
  }

  ngOnChanges() {
    this.icon = this.stateToIcon[this.stateString];
  }
}
