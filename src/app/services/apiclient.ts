import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';

import {OpenID} from './openid';


@Injectable()
export class APIClient {
  headers: Headers;
  url: string;

  constructor(private http: Http, private openid: OpenID) {
    this.headers = openid.getHeaders();
    this.url = "https://api.creator.no/members/";
  }

  setHeaders(headers: Object) {
    for (var k in headers) {
      this.headers.set(k, headers[k]);
    }
  }

  get(path: string) {
    return this.http.get(this.url + path, { headers: this.headers });
  }

  post(path: string, data?: Object) {
    let json = JSON.stringify(data);
    return this.http.post(this.url + path, json, { headers: this.headers });
  }
}
