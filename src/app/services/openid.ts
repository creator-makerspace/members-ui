import {Injectable} from 'angular2/core';
import {Headers, Http} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Observable, Subject} from "rxjs";

import {JwtHelper} from 'angular2-jwt/angular2-jwt';

export interface IOpenIdConfig {
  url: string;
  clientId: string;
  state: string;
  nonce: string;
  redirectUri: string;
  responseType: Array<string>;
  realm: string;
  scope: Array<string>;
  authorizationEndpoint: string;
  tokenEndpoint: string;
  userinfoEndpoint: string;
  discoveryEndpoint: string;
  tokenName: string;
}

export class OpenIdConfig {
  url: string;
  clientId: string;
  state: string;
  nonce: string;
  redirectUri: string;
  responseType: Array<string>;
  realm: string;
  scope: Array<string>;
  authorizationEndpoint: string;
  tokenEndpoint: string;
  userinfoEndpoint: string;
  discoveryEndpoint: string;
  tokenName: string;

  constructor(cfg?: any) {
    this.url = cfg.url;
    this.clientId = cfg.clientId;
    this.state = cfg.state;
    this.nonce = cfg.nonce;
    this.redirectUri = cfg.redirectUri;
    this.responseType = cfg.responseType || ["id_token"];
    this.realm = cfg.realm || "/";
    this.scope = cfg.scope || ["token"];
    this.authorizationEndpoint = cfg.authorizationEndpoint;
    this.tokenEndpoint = cfg.tokenEndpoint;
    this.userinfoEndpoint = cfg.userinfoEndpoint;
    this.discoveryEndpoint = cfg.discoveryEndpoint || "/.well-known/openid-configuration";
    this.tokenName = cfg.tokenName || "idToken";
  }
}

export interface Jwt {
  claims: Object;
  headers: any;
  signature: string;
  raw: string;
}

@Injectable()
export class OpenID {
  config: OpenIdConfig;
  helper = new JwtHelper();

  userInfo: Observable<Object>;

  tokenStream: Subject<Jwt> = new Subject();
  jwt: Jwt = null;

  constructor(public options: OpenIdConfig, private http: Http) {
    this.config = options;

    this.config.redirectUri = this.getWindowBaseUrl() + "/openid";
    //this.options.redirectUri = this.getWindowBaseUrl() + "/index.html"*/

    this.tokenStream.subscribe(
      jwt => {
        this.jwt = jwt;
      }
    );

    this.tokenStream.next(this.loadFromStorage());
  }

  loadFromStorage(): Jwt {
    let tokenStr = sessionStorage.getItem(this.config.tokenName);

    if (tokenStr !== null) {
      return this.loadToken(tokenStr);
    }

    return null;
  }

  getWindowBaseUrl() {
    return window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;
  }

  getBaseUrl() {
    if (this.config.url === null) {
      return this.getWindowBaseUrl();
    }

    return this.config.url;
  }

  /*
    Get parameters used for formatting the Authorize url
  */
  getAuthorizedParams() {
    return {
      client_id: this.config.clientId,
      realm: this.config.realm,
      response_type: this.config.responseType.join(" "),
      scope: this.config.scope.join(" "),
      redirect_uri: this.config.redirectUri,
      state: this.config.state,
      nonce: this.config.nonce,
    };
  }

  /*
    Format the URL used when authing towards the OP
  */
  getAuthorizeUrl(): string {
    var query = this.encodeQueryData(this.getAuthorizedParams());

    var authUrl = this.config.authorizationEndpoint;
    if (this.config.authorizationEndpoint.startsWith("/")) {
      authUrl = this.getBaseUrl() + authUrl;
    }

    return authUrl + "?" + query;
  }

  /*
    This will attempt to fetch the discovery document of the IDP
  */
  discover(): Observable<any> {
    let url = this.getBaseUrl() + this.config.discoveryEndpoint;

    let headers = new Headers();
    headers.set("Accept", "application/json");

    return this.http.get(url, { headers: headers })
      .map(resp => resp.json())
      .do(
      data => {
        this.config.authorizationEndpoint = data.authorization_endpoint;
        this.config.tokenEndpoint = data.token_endpoint;
        this.config.userinfoEndpoint = data.userinfo_endpoint;
      }
      );
  }

  /*
    Get the response from the OP
  */
  getParamsFromFragment(): Object {
    var params = {};
    var postBody = location.search.substring(1);
    var regex = /([^&=]+)=([^&]*)/g, m;

    while (m = regex.exec(postBody)) {
      params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

    return params;
  }

  /*
    Encode data when going to the OP
  */
  encodeQueryData(data): string {
    var ret = [];

    for (var d in data) {
      ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
    }

    return ret.join("&");
  }

  loadToken(token: string): Jwt {
    let parts = token.split(".");

    let jwt = {
      claims: this.helper.decodeToken(token),
      headers: JSON.parse(this.helper.urlBase64Decode(parts[0])),
      signature: parts[2],
      raw: token
    };

    var self = this;

    // Assert if the token has been expired, then we'll need to logout...
    let timeLeft = jwt.claims.exp - jwt.claims.iat;

    if (timeLeft <= 0) {
      this.logOut();
      return null;
    }

    // Set a timeout that calls logout when the token is expired
    Observable.timer(timeLeft * 1000).subscribe(() => self.logOut);

    return jwt;
  }

  /* Loads the token into storage and makes the info available */
  setTokenFromQuery(): void {
    var p = this.getParamsFromFragment();

    var token = p["id_token"];

    if (token !== null)  {
      let jwt = this.loadToken(token);

      sessionStorage.setItem(this.config.tokenName, jwt.raw);
      this.tokenStream.next(jwt);
    }
  }

  logOut(): void {
    sessionStorage.removeItem(this.config.tokenName);
    // Set the token to NULL after logging out
    this.tokenStream.next(null);
  }

  // Fetches user info based on the idtoken etc... Not used atm
  getUserInfo() {
    let url = this.getBaseUrl() + this.config.userinfoEndpoint;
    this.userInfo = this.http.get(url, { headers: this.getHeaders() });
    return this.userInfo;
  }

  getHeaders() {
    var headers = new Headers();
    headers.set("Content-Type", "application/json");
    headers.set("Authorization", "Bearer " + this.jwt.raw);
    return headers;
  }
}
