import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Subject} from "rxjs";

import {OpenID} from './openid';

@Injectable()
export class ProfileService {
  current: Subject<any>;

  constructor(private http: Http, private openid: OpenID) {
    this.current = new Subject();

    this.openid.tokenStream.subscribe(
      token => {
        if (token === null) {
          this.current.next({});
        }
      }
    );
  }

  refreshProfile() {
    this.http.get(
      "https://api.creator.no/members/profile",
      { headers: this.openid.getHeaders() })
    .map(response => response.json())
    .subscribe(data => this.current.next(data));

    return this.current;
  }
}
