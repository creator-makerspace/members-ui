import {Component, View} from 'angular2/core';
import {Route, RouterLink, RouteConfig} from 'angular2/router';

import '../style/app.scss';

import {Sidebar} from './components/sidebar/sidebar';
import {Navbar} from './components/navbar/navbar';

import {Home} from './components/home/home';
import {About} from './components/about/about';
import {Profile} from './components/profile/profile';

import {LoggedInRouterOutlet} from './components/auth/auth';
import {OpenIDCmpt} from './components/auth/openid';
import {Login} from './components/auth/login';
import {Logout} from './components/auth/logout';
import {Signup} from './components/signup/signup';

import {OpenID} from './services/openid';
import {ProfileService} from './services/profile';

@Component({
  selector: 'app',
  providers: [OpenID, ProfileService]
})
@View({
  styles: [require("./app.scss")],
  template: require('./app.html'),
  directives: [LoggedInRouterOutlet, RouterLink, Sidebar, Navbar],
})
@RouteConfig([
  new Route({ path: '/', component: Home, name: 'Home', useAsDefault: true }),
  new Route({ path: '/about', component: About, name: 'About' }),
  new Route({ path: '/profile/...', component: Profile, name: 'Profile' }),
  new Route({ path: '/signup', component: Signup, name: 'Signup' }),
  new Route({ path: '/login', component: Login, name: 'Login' }),
  new Route({ path: '/logout', component: Logout, name: 'Logout' }),
  new Route({ path: '/openid', component: OpenIDCmpt, name: 'OpenID' })
])
export class Application {
  constructor(private openid: OpenID, private profileSvc: ProfileService) {
    // If we're authed we fetch the profile for the whole app to use.
    openid.tokenStream.subscribe(
      jwtToken => {
        if (jwtToken) {
          profileSvc.refreshProfile();
        }
      }
    );

    // And then if the state is that we're authed when we're constructed we load as well
    if (openid.jwt !== null) {
      profileSvc.refreshProfile();
    }
  }
}
