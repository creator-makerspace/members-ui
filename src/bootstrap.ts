//<reference path="../node_modules/angular2/typings/browser.d.ts"/>

import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS, Http} from 'angular2/http';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {provide} from 'angular2/core';

import {Application} from './app/app';
import {OpenID, OpenIdConfig} from './app/services/openid';

import {Ng2BootstrapConfig, Ng2BootstrapTheme} from 'ng2-bootstrap/ng2-bootstrap';
Ng2BootstrapConfig.theme = Ng2BootstrapTheme.BS4;

// Hack since html routing mode is enabled...
var hash = location.hash.substring(1);
if (hash.search("token") !== -1) {
  location.search = hash;
}

//require("bootstrap!dist!css!./bootstrap.min.css");

var oidcConfig: any = require("../oidc.json");

bootstrap(Application,
  [
    HTTP_PROVIDERS,
    ROUTER_PROVIDERS,
    provide(OpenIdConfig, {
      useValue: new OpenIdConfig(oidcConfig)
    }),
    provide(OpenID, {
      useFactory: (config, http) => {
        return new OpenID(config, http);
      },
      deps: [Http, OpenIdConfig]
    })
  ])
  .catch(err => console.error(err));
